from setuptools import setup

setup(name="grit_blockchain_api",
      version='0.1',
      description='Package used for doing blockchain  operations',
      url='http://ilozuluchris@bitbucket.org/grit_sw/grit_blockchain_api.git',
      author='Ilozulu Chidiuso',
      author_email='chidiuso@grit.systems',
      classifiers=[
        'Development Status :: 1 - Alpha',
        # todo use correct license 'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Topic :: Blockchain development',
      ],
      license='MIT',
      packages=['grit_blockchain_api'],
      install_requires=[
          'eth-account==0.3.0',
          'web3==4.8.2',
      ],
      zip_safe=False)
