from grit_blockchain_api import EventListener
from config import (ESCROW_ADDRESS, INFURA_SECRET, OWNER_ACCOUNT, TOKEN_ADDRESS)
if __name__ == "__main__":
    EventListener(network="ropsten", token_address=TOKEN_ADDRESS, escrow_address=ESCROW_ADDRESS,
                  owner_account=OWNER_ACCOUNT, key=INFURA_SECRET).start()
