"""
Module containing the fundamental fxns that other classes use.
"""
from ast import literal_eval
import os

from web3 import Web3

from .utils import get_contract_details, is_connected

PACKAGE_ROOT = os.path.abspath(os.path.dirname(__file__))


class Base:
    """
    Base class for  blockchain apis that every other class inherits from.
    It connects to the infura using the network and infura key passed in.
    It also ensures the connection works.
    """
    def __init__(self, key, network, contract_address, contract_name, owner_account=None):
        self.network = network
        self.key = key
        self.contract_name = contract_name
        self.infura_domain = f'https://{self.network}.infura.io'
        self.contract_address = contract_address
        self.owner_account = owner_account
        self.w3 = Web3(Web3.HTTPProvider(f'{self.infura_domain}/v3/{self.key}'))
        self._is_connected()

        self.contract = self.w3.eth.contract(address=self.w3.toChecksumAddress(contract_address),
                                             abi=self._get_contract_details(contract_name=self.contract_name)['abi'])

    @staticmethod
    def _get_contract_details(contract_name):
        return get_contract_details(contract_name)

    def _is_connected(self):
        return is_connected(self)

    # todo: would this be better as a decorator
    def _run_transaction(self, txn, private_key):
        """
        Sign transaction and send to infura to broadcast to other nodes.
        Note: this method is not needed for read operations on the blockchain.
        :param txn: Details about transaction
        :type txn: dict
        :param private_key: private key of Ethereum address conducting transaction
        :type private_key: str
        :return: str containing url to check status of transaction.
        """
        signed_txn = self.w3.eth.account.signTransaction(txn, private_key)
        trans_id = self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)
        return f"Check status of transaction at https://{self.network}.etherscan.io/tx/{trans_id.hex()}"

