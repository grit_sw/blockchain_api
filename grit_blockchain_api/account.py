"""
This module handles all ethereum account activities such as creation of accounts.
"""
from eth_account import Account

# todo check documentation of fxns according to pep8


def make_new_account():
    """
    Create a new ethereum account
    :return: dict - containing privatekey and address
            :rtype dict['private_key'] bytes
            :rtype dict['address'] Str
    """
    acct = Account.create(extra_entropy="old solider neva die except in 9ja!")
    account_dict = {'private_key': acct.privateKey.hex(), 'address': acct.address}
    # todo should private key be encrypted first
    return account_dict
