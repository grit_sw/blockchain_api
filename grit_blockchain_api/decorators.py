"""
Module containing all decorators used.
"""


def owner_only(func):
    def wrapper(self, *args, **kwargs):
        if not self.owner_account:
            return
        return func(self, *args, **kwargs)
    return wrapper


def escrow_needed(func):
    def wrapper(self, *args, **kwargs):
        if self.escrow_address is None:
            return "Escrow contract is needed for this!"
        return func(self, *args, **kwargs)
    return wrapper


def verify_account_format(func):
    def wrapper(self, *args, **kwargs):
        account = kwargs.get('account')
        try:
            if account['address'] and account['private_key']:
                return func(self, *args, **kwargs)
        except KeyError:
            return
    return wrapper
