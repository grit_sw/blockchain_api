"""
Module that contains fxns for interacting with the grit
"""
from .decorators import owner_only
from .base import Base


class GRITEscrow(Base):
    def __init__(self, key, network, contract_address, owner_account=None):
        super().__init__(key, network, contract_address, "GritEscrow", owner_account)

    @owner_only
    def new_order(self, order_id, buyer_address, seller_address, estimated_value, other_details="", actual_value=0):
        try:
            estimated_value = int(estimated_value)
        except (ValueError, TypeError):
            return f'Estimated_value:{estimated_value} could not be serialized to integer'
        nonce = self.w3.eth.getTransactionCount(self.owner_account['address'])
        new_order_txn = self.contract.functions.newOrder(int(order_id), buyer_address, seller_address, estimated_value, actual_value, other_details).buildTransaction({'nonce': nonce, 'gas': 1000000})
        return self._run_transaction(txn=new_order_txn, private_key=self.owner_account['private_key'])

    @owner_only
    def release(self, order_id, actual_value):
        try:
            actual_value = int(actual_value)
        except (ValueError, TypeError):
            return f'Actual_value:{actual_value} could not be serialized to integer'
        nonce = self.w3.eth.getTransactionCount(self.owner_account['address'])
        release_txn = self.contract.functions.release(order_id, actual_value).buildTransaction({'nonce': nonce, 'gas': 100000})
        return self._run_transaction(txn=release_txn, private_key=self.owner_account['private_key'])

    @owner_only
    def approve_refund(self, order_id):
        nonce = self.w3.eth.getTransactionCount(self.owner_account['address'])
        approve_refund_txn = self.contract.functions.approveRefund(order_id).buildTransaction({'nonce': nonce, 'gas': 100000})
        return self._run_transaction(txn=approve_refund_txn, private_key=self.owner_account['private_key'])

    def collect_refund(self, order_id, buyer):
        nonce = self.w3.eth.getTransactionCount(buyer['address'])
        refund_txn = self.contract.functions.refund(order_id).buildTransaction({'nonce': nonce, 'gas': 100000})
        return self._run_transaction(txn=refund_txn, private_key=buyer['private_key'])

    def deposit(self, order_id, buyer):
        nonce = self.w3.eth.getTransactionCount(buyer['address'])
        deposit_txn = self.contract.functions.deposit(order_id).buildTransaction({'nonce': nonce, 'gas': 100000})
        return self._run_transaction(txn=deposit_txn, private_key=buyer['private_key'])

    def withdraw(self, order_id, seller):
        nonce = self.w3.eth.getTransactionCount(seller['address'])
        withdraw_txn = self.contract.functions.withdraw(order_id).buildTransaction({'nonce': nonce, 'gas': 100000})
        return self._run_transaction(txn=withdraw_txn, private_key=seller['private_key'])
