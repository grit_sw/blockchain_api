"""
This event contains code for listening to events emitted by grit smart contracts
"""
import asyncio
from threading import Thread

import time

from itertools import chain
from web3 import Web3

from .gritescrow import GRITEscrow
from .gritcoin import GRITCoin
from .utils import get_contract_details, is_connected, status_tuple

from logger import logger
from utils import get_user_group_with_blockchain_address, get_transaction, get_blockchain_account, credit_group, \
    debit_group

buyer = {'private_key': '0xae09851f8dab79b4ed494f1b169e7c024c0c62108d6ec997599ec0aa85c4bc5e',
         "address": '0x9A5A7679619fF0C81fb546174F09e471E7fa22f7'}

seller = {'private_key': '0xfb4c754a726142416a41e416e6348e1f69812027a20d88c2e764f194d6297358',
          'address': '0x2A5017D334aEd1A9f93BBe2889fe09f8c1a0918d'}


class EventListener(Thread):
    def __init__(self, network, token_address, escrow_address, key=None, owner_account=None):
        self.network = network
        self.infura_domain = f'wss://{self.network}.infura.io/ws'
        self.w3 = Web3(Web3.WebsocketProvider(self.infura_domain))
        self._is_connected()

        self.token_address = self.w3.toChecksumAddress(token_address)
        self.escrow_address = self.w3.toChecksumAddress(escrow_address)
        self.token_contract = self.w3.eth.contract(address=self.token_address,
                                                   abi=get_contract_details("GritCoin")['abi'])
        self.escrow_contract = self.w3.eth.contract(address=self.escrow_address,
                                                    abi=get_contract_details("GritEscrow")['abi'])
        if key:
            self.grit_escrow = GRITEscrow(key=key, network=network, contract_address=escrow_address, owner_account=owner_account)
            self.grit_token = GRITCoin(key=key, network=network, contract_address=token_address,
                                       owner_account=owner_account, escrow_address=escrow_address)
        self.token_transfer_filter = self.token_contract.events.Transfer.createFilter(fromBlock="latest")

        self.ordercreation_filter = self.escrow_contract.events.OrderCreation.createFilter(fromBlock="latest")

        self.orderstatuschange_filter = self.escrow_contract.events.OrderStatusChange.createFilter(fromBlock="latest")

        self.refundapproved_filter = self.escrow_contract.events.RefundApproved.createFilter(fromBlock="latest")

        self.token_approval_filter = self.token_contract.events.Approval.createFilter(fromBlock="latest")

        self.event_filters = [self.token_approval_filter, self.token_transfer_filter, self.orderstatuschange_filter,
                              self.ordercreation_filter, self.refundapproved_filter]
        super().__init__()

    def _is_connected(self):
        return is_connected(self)

    def _handle_new_order(self, event):
        data_emitted = event.args
        order_id = int(data_emitted.order_id)
        buyer_address = data_emitted.buyer
        buyer_account = get_user_group_with_blockchain_address(buyer_address)
        if buyer_account:
            url = self.grit_escrow.deposit(order_id, buyer=buyer_account['blockchain_account'])
            logger.info(f"Deposit transaction:{url}")

    def handle_event(self, event):
        logger.info(event)
        event_name = event.event
        if event_name == "Transfer":
            self._handle_transfer(event)
        if event_name == "OrderCreation":
            self._handle_new_order(event)

        if event_name == "Approval":
            self._handle_approval_event(event)

        if event_name == "OrderStatusChange":
            self._handle_order_status_event(event)

        if event_name == "RefundApproved":
            self._handle_refund_approve_event(event)

    def _handle_refund_approve_event(self, event):
        data_emitted_by_event = event.args
        order_id = data_emitted_by_event.order_id
        buyer_account = self.get_buyer_or_seller_blockchain_account(order_id, role='buyer')
        if buyer_account:
            status_url = self.grit_escrow.collect_refund(order_id=order_id, buyer=buyer_account)
            logger.info(f"Refunded transaction: {status_url}, dont try withdraw from order with id: {order_id}")

    @staticmethod
    def get_buyer_or_seller_blockchain_account(order_id, role):
        transaction = get_transaction(int(order_id))
        if transaction:
            try:
                id_to_use = transaction[f'{role}_id']
            except KeyError:
                logger.info(f"Transaction was found but buyer_id(a key of the json returned) could not be got")
                return
            account = get_blockchain_account(id_to_use)
            return account

    def _handle_approval_event(self, event):
        # todo Allow user create buy order  maybe via notifications
        address = event.args.spender #address can be used to pick user
        value = event.args.value
        user = get_user_group_with_blockchain_address(address)
        if user:
            logger.info(f"Escrow can spend {value} GCN from {address}, user with id of {user['group_id']} can place order")
        pass

    def _handle_order_status_event(self, event):
        data_emitted_by_event = event.args
        order_status = int(data_emitted_by_event.status)
        order_id = data_emitted_by_event.order_id
        if order_status == 1:
            logger.info(f"Order with id:{order_id} has been {status_tuple[1].lower()}, "
                  f"seller of order would soon receive funds, "
                  f"don't run any other operations on this order eg: refunding, depositing etc.")
            seller_account = self.get_buyer_or_seller_blockchain_account(order_id, role='seller')
            if seller_account:
                status_url = self.grit_escrow.withdraw(order_id=order_id, seller=seller)
                logger.info(f'Withdrawal transaction: {status_url}')
        if order_status == 2:
            logger.info(f"The buyer of order with id:{order_id} has been {(status_tuple[2]).lower()}, don't run any other operations on this order eg: withdrawing, depositing etc")
            # todo should sth happen
        if order_status == 3:
            logger.info(f"Order with id {order_id} has status of {status_tuple[3]}, don't create any other order with this id:{order_id}.")

    def _handle_transfer(self, event):
        data_emitted = event.args
        sender = data_emitted['from']
        receiver = data_emitted['to']
        naira_to_send = data_emitted['value']  # since 1 gcn is 1 kobo
        if sender == "0x0000000000000000000000000000000000000000":
            logger.info("That was minting")
            minted_user = get_user_group_with_blockchain_address(receiver)
            if minted_user:
                minted_user_blockchain_account = minted_user['blockchain_account']
                minted_group_id = minted_user['group_id']
                debit_group(group_id=minted_group_id, amount=naira_to_send)
                # todo is not  better to approve spending for a very large quantity
                status_url = self.grit_token.approve_spending(value=int(data_emitted.value),
                                                              account=minted_user_blockchain_account)
                logger.info(f'Approve spending url:{status_url}')

        if receiver == "0x0000000000000000000000000000000000000000":
            logger.info("That was burning!")
            user = get_user_group_with_blockchain_address(sender)
            if user:
                user_id = user['group_id']
                logger.info(f"Send {naira_to_send} naira to user with id of {user_id}")
                credit_group(group_id=user_id, amount=naira_to_send) # to do maybe when transactions can be retried(or reconstructed) this would be done before it gets to the blockchain

        if sender == self.escrow_address:
            logger.info("GRIT coin was sent to buyer(if a refund was approved) "
                  "or sent to seller(if order was fulfilled buy seller)")
            #todo notify user
            user = get_user_group_with_blockchain_address(receiver)
            if user:
                user_id = user['group_id']
                logger.info(f"Notify user with id of {user_id}")

    # def log_loop(self, event_filter, poll_interval=15):
    #         logger.info("Logging.............")
    #         try:
    #             for event in event_filter.get_new_entries():
    #                 self.handle_event(event)
    #             time.sleep(poll_interval)
    #         except:
    #             pass
    def work(self):
        while True:
            try:
                new_events = [event_filter.get_new_entries() for event_filter in self.event_filters]
                new_events = list(chain.from_iterable(new_events))
                logger.info(f"After flattening:{new_events}")
                if new_events:
                    for event in new_events:
                        self.handle_event(event)
            except Exception as e:
                logger.info(f"Error gotten was {str(e)}")
                continue
            time.sleep(60)



    def run(self):
        logger.info("Starting to listen to all events")
        try:
            self.work()
        except Exception as e:
            logger.info(str(e))
            self.work()

