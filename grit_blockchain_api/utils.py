import json
import os

PACKAGE_ROOT = os.path.abspath(os.path.dirname(__file__))


def get_contract_details(contract_name):
    """
    Get details about contract passed
    :param contract_name:
    :param path:
    :return:
    """
    useful_details = {}
    with open(f'{PACKAGE_ROOT}/abi/{contract_name}.json') as fh:
        contract_details = json.load(fh)
    try:
        useful_details['abi'] = contract_details['abi']
        # useful_details['byte_code'] = contract_details['bytecode']
        # useful_details['address'] = contract_details['networks']['5777']['address']
    except KeyError:
        raise Exception("Could not find contract abi")
    return useful_details


def is_connected(self):
    """
    Raises an exception if the connection did not work
    """
    if not self.w3.isConnected():
        raise Exception(f'Could not connect to {self.infura_domain}')


status_tuple = ('Pending', 'Completed', 'Refunded', 'Deposited')
