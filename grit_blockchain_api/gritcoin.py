"""
Module that contains fxns for interacting with the grit token (Grit Coin)
"""
from .base import Base
from .decorators import owner_only, escrow_needed


class GRITCoin(Base):
    def __init__(self, key, network, contract_address, owner_account=None, escrow_address=None):
        self.escrow_address = escrow_address
        super().__init__(key, network, contract_address, "GritCoin", owner_account)

    # todo can this function as an property
    def balance(self, address):
        """
        Get amount of grit coin held by the address passed in.
        :param address: ethereum address to query for amount of grit token in
        :type address: str
        :return: Int amount of grit coin held by address
        """
        return self.contract.functions.balanceOf(address).call()

    @owner_only
    def mint(self, address, amount):
        """
        Create  amount of grit token and send to account passed in
        :param address: ethereum address of receiver
        :type address: str
        :param amount: Amount of grit token to be sent to receiver. Note the decimals of grit token is 2.
                       That means to send 1 grit token pass in 100(1*10^2)
        :type amount: Int
        :return: Status string containing url to see the status of transaction
        """
        nonce = self.w3.eth.getTransactionCount(self.owner_account['address'])
        mint_txn = self.contract.functions.mint(address, amount).buildTransaction({'nonce': nonce, 'gas': 500000})
        return self._run_transaction(txn=mint_txn, private_key=self.owner_account['private_key'])

    @owner_only
    def burn(self, address, amount):
        """
        Destroy amount of grit token and present in accounts passed in.
        :param address: ethereum account that has tokens to be destroyed
        :type address: str
        :param amount: Amount of grit token to be burnt. Note the decimals of grit token is 2.
                       That means to burn 1 grit token pass in 100(1*10^2)
        :type amount: Int
        :return: Status string containing url to see the status of transaction
        """
        nonce = self.w3.eth.getTransactionCount(self.owner_account['address'])
        burn_txn = self.contract.functions.burn(address, amount).buildTransaction({'nonce': nonce, 'gas': 500000})
        return self._run_transaction(txn=burn_txn, private_key=self.owner_account['private_key'])

    @escrow_needed
    def approve_spending(self, value, account):
        nonce = self.w3.eth.getTransactionCount(account['address'])
        approve_txn = self.contract.functions.approve(self.w3.toChecksumAddress(self.escrow_address), value).buildTransaction(
            {'nonce': nonce, 'gas': 50000})
        return self._run_transaction(txn=approve_txn, private_key=account['private_key'])
