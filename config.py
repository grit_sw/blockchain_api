OWNER_ADDRESS = "0x4a93ACB523f16e02E6C15a737923286B94074Bb3"

OWNER_PRIVATE_KEY = b'\x1d\xf8N{CTb\xaeW(\x06\xa1\xac\xafN\xe9I\xa8w\x05\x83\x10<O\xb8\xd1\xa9\xe0@<=G'

OWNER_ACCOUNT = {'address': OWNER_ADDRESS, 'private_key': OWNER_PRIVATE_KEY }

ESCROW_ADDRESS = "0x2481d28e6e16b43fc8573d121d892ad643072bb7"

TOKEN_ADDRESS = "0x95A92B3F7F845AEC0D7aFc5aAD9fC3E922F4ca21"

INFURA_SECRET = "526034df748444d2b2d9ad801d577b64"

INFURA_NETWORK = "ropsten"

BUYER = {'private_key': '0xae09851f8dab79b4ed494f1b169e7c024c0c62108d6ec997599ec0aa85c4bc5e',
         "address": '0x9A5A7679619fF0C81fb546174F09e471E7fa22f7'}

# todo move to environment file so docs file can work well
USERS_API_URI = 'http://users_api:5000'

TRANSACTION_API_URI = 'http://transaction_api:5000'

ACCOUNT_API_URI = 'http://account_api:5000'
