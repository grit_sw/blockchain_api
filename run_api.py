from typing import NamedTuple

from arrow import now
from flask import Flask, jsonify, request
from flask_restplus import Api, fields, Resource

from grit_blockchain_api import (make_new_account, GRITCoin, GRITEscrow)
from logger import logger

# todo: move this to environment file
# ideally both the escrow and token contract belong to the same account
from config import (OWNER_ACCOUNT, ESCROW_ADDRESS, TOKEN_ADDRESS,
                    INFURA_SECRET, INFURA_NETWORK, BUYER, USERS_API_URI)
from utils import get_blockchain_account, enough_balance

app = Flask(__name__)
api = Api(doc='/doc/')
api.init_app(app)
#models

new_transaction = api.model('NewTransaction', {
    'order_id': fields.Integer,
    'buyer_id': fields.String,
    'seller_id': fields.String,
    'estimated_value': fields.Integer,
    'other_details': fields.String(required=False),
})

complete_transaction = api.model('CompleteTransaction', {
    'actual_value': fields.Integer,
})

@app.after_request
def log_info(response):
	try:
		log_data = {
			'connection': request.headers.get('Connection'),
			'ip_address': request.remote_addr,
			'browser_name': request.user_agent.browser,
			'user_device': request.user_agent.platform,
			'referrer': request.referrer,
			'request_url': request.url,
			'host_url': request.host_url,
			'status_code': response.status_code,
			'date': str(now('Africa/Lagos')),
			'location': response.location,
		}
		logger.info('blockchain_api_logs : {}'.format(log_data))
	except Exception as e:
		logger.exception("blockchain_api_logs: {}".format(e))
	return response

@api.route('/new-account')
class NewAccount(Resource):
    def get(self):
        """
        Creating new blockchain accounts
        :return: Privatekey and address
        """
        response = {}
        try:
            account = make_new_account()
            response['success'] = True
            response['data'] = account
            response['message'] = "Blockchain account created"
        except Exception as e:
            response['success'] = False
            response['message'] = f"Blockchain account not created, error encountered: {str(e)}"
        return jsonify(response)


# todo validate address using marshmallow or custom func
@api.route("/balance/<string:user_id>")
class CheckBalance(Resource):
    def get(self, user_id):
        """
        Getting a user's GCN balance
        :param user_id:
        :return:
        """
        response = {}
        blockchain_account = get_blockchain_account(user_id)
        if blockchain_account:
            try:
                token = GRITCoin(key=INFURA_SECRET, network=INFURA_NETWORK, contract_address=TOKEN_ADDRESS,
                                 owner_account=OWNER_ACCOUNT)
                response['data'] = {'balance': token.balance(blockchain_account['address'])}
                response['message'] = "Balance of Grit token gotten"
                response['success'] = True
            except Exception as e:
                response['message'] = str(e)  # todo is it better to return a generic failure and log error instead
                response['success'] = False
        else:
            response['message'] = f'User with id {user_id}, does not have a blockchain account one would be generated'
            response['success'] = False
        return jsonify(response)


@api.route('/mint/<string:group_id>/<int:amount>')
class Mint(Resource):
    def post(self, group_id, amount):
        """
        Endpoint for adding new GCN to a user's blockchain address
        :param group_id:
        :param amount:
        :return:
        """
        response = {}
        blockchain_account = get_blockchain_account(group_id)
        if blockchain_account and enough_balance(group_id=group_id, amount=amount):
            try:
                token = GRITCoin(key=INFURA_SECRET, network=INFURA_NETWORK, contract_address=TOKEN_ADDRESS,
                                 owner_account=OWNER_ACCOUNT)
                status_url = token.mint(address=blockchain_account['address'], amount=amount)
                response['success'] = True
                response['data'] = {'status_url': f"Click on {status_url} to view state of mint GRIT coin transaction"}
                response['message'] = f"Group with id of {group_id} would soon receive {amount} GRIT token"
            except Exception as e:
                response['success'] = False
                response['message'] = str(e)
        else:
            response['message'] = f'Group with id {group_id}, does not have a blockchain account one would be generated, try again soon'
            response['success'] = False
        return jsonify(response)


@api.route('/burn/<string:group_id>/<int:amount>')
class Burn(Resource):
    def post(self, group_id, amount):
        """
        Endpoint for burning GCNs to  a user's account

        :param group_id:
        :param amount:
        :return:
        """
        response = {}
        blockchain_account = get_blockchain_account(group_id)
        if blockchain_account:
            try:
                token = GRITCoin(key=INFURA_SECRET, network=INFURA_NETWORK, contract_address=TOKEN_ADDRESS,
                                 owner_account=OWNER_ACCOUNT)
                status_url = token.burn(address=blockchain_account['address'], amount=amount)
                response['data'] = {'status_url': f"Click on {status_url} to view state of burn GRIT coin transaction."}
                response['message'] = f"{amount} GRIT token would soon be removed from user with id of {group_id}."
                response['success'] = True
            except Exception as e:
                response['message'] = str(e)
                response['success'] = False
        else:
            response['message'] = f'Group with id {group_id}, does not have a blockchain account one would be generated, try again soon.'
            response['success'] = False
        return jsonify(response)


@api.route('/new-order',)
class Order(Resource):
    @api.expect(new_transaction)
    def post(self):
        """
        Creating new blockchain orders
        :return:
        """
        response = {}
        payload = request.json
        other_details = ""
        try:
            order_id = payload['order_id']
            buyer_id = payload['buyer_id']
            seller_id = payload['seller_id']
            estimated_value = payload['estimated_value']  # since one grit coin is one kobo
            try:
                other_details = payload['other_details']
            except KeyError:
                pass  # other_details is optional
        except KeyError:
            response['message'] = f"Ensure the json passed has the following keys buyer, seller, estimated_value." \
                                  f" The other_details key is optional, json passed was {payload}"
            response['success'] = False
            return jsonify(response)

        buyer_account = get_blockchain_account(buyer_id)
        seller_account = get_blockchain_account(seller_id)
        if buyer_account and seller_account:
            if enough_balance(group_id=buyer_id, amount=estimated_value):
                try:
                    escrow = GRITEscrow(key=INFURA_SECRET, network=INFURA_NETWORK, contract_address=ESCROW_ADDRESS,
                                        owner_account=OWNER_ACCOUNT)
                    status_url = escrow.new_order(order_id=order_id, buyer_address=buyer_account['address'],
                                                  seller_address=seller_account['address'], estimated_value=estimated_value, other_details=other_details)
                    response['data'] = {'status_url': f"Click on {status_url} to view state of order creation transaction."}
                    response['success'] = True
                    response['message'] = "A new order would soon be created."
                except Exception as e:
                    response['success'] = False
                    response['message'] = str(e)
            else:
                response['success'] = False
                response['message'] = f"Buyer does not have enough balance to carry ot a transaction" \
                                      f" with this value: {estimated_value}"
        else:
            response['success'] = False
            response['message'] = f"Either buyer or seller(or both) should have a valid blockchain account," \
                                  f" valid blockchain accounts would be added try again " \
                                  f"buyer account was {buyer_account} and seller account was {seller_account}"
        return jsonify(response)


@api.route('/complete-order/<int:order_id>')
#todo check that order status change of 3 was recorded save order status of three 3 with order id in db
class CompleteOrder(Resource):
    @api.expect(complete_transaction)
    def post(self, order_id):
        """
        Completing new orders
        :param order_id:
        :return:
        """
        response = {}
        payload = request.json
        try:
            actual_value = payload['actual_value']
        except KeyError:
            response['message'] = f"Ensure the json passed has the key actual_value"
            response['success'] = False
            return jsonify(response)
        try:
            escrow = GRITEscrow(key=INFURA_SECRET, network=INFURA_NETWORK, contract_address=ESCROW_ADDRESS,
                                    owner_account=OWNER_ACCOUNT)
            status_url = escrow.release(order_id=order_id, actual_value=actual_value)
            response['data'] = {'status_url': f"Click on {status_url} to view state of complete order transaction."}
            response['success'] = True
            response['message'] = f"Order of id {order_id} has been fulfilled by seller." \
                                  f"Seller would soon receive funds"
        except Exception as e:
            response['success'] = False
            response['message'] = str(e)

        return jsonify(response)


@api.route('/approve-refund/<int:order_id>')
class ApproveRefund(Resource):
    def post(self, order_id):
        """
        Approving a refund for an order.
        :param order_id:
        :return:
        """
        response = {}
        try:
            escrow = GRITEscrow(key=INFURA_SECRET, network=INFURA_NETWORK, contract_address=ESCROW_ADDRESS,
                                    owner_account=OWNER_ACCOUNT)
            status_url = escrow.approve_refund(order_id=order_id)
            response['data'] = {'status_url': f"Click on {status_url} to view state of approve refund transaction."}
            response['success'] = True
            response['message'] = f"Refund approved, buyer of order with id {order_id} would soon get funds back"
        except Exception as e:
            response['success'] = False
            response['message'] = str(e)

        return jsonify(response)
