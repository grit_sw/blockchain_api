"""Module containing miscellaneous functions"""
import random
from string import ascii_letters

import requests
import urllib3
from flask import jsonify
from web3 import Web3

from config import ACCOUNT_API_URI, INFURA_SECRET, INFURA_NETWORK, TRANSACTION_API_URI, USERS_API_URI
from grit_blockchain_api import make_new_account
from logger import logger


def connect_to_node(retries=0):
    """
    Try to connect to ethereum node
    :param retries: Number of times to connect to node
    :type retries: int
    :return:
    """
    w3 = Web3(Web3.HTTPProvider(f'https://{INFURA_NETWORK}.infura.io/v3/{INFURA_SECRET}'))
    if retries < 3:
        if w3.isConnected():
            return w3
        else:
            connect_to_node(retries=retries+1)
    else:
        raise Exception("Could not conect")


def get_blockchain_transaction_receipt(transaction_hash):
    """
    Get receipt of transaction using transaction hash passed in.
    :param transaction_hash: hash gotten from sending transaction to node
    :return: Receipt of transaction
    :rtype: None or AttributeDict
    """
    w3 = connect_to_node()
    return w3.eth.getTransactionReceipt(transaction_hash=transaction_hash)


def get_blockchain_transaction_status(transaction_hash):
    """
    Get status of transaction with transaction hash passed in.
    :param transaction_hash:
    :return: String reporting status of transaction
    :rtype: str
    """
    receipt = get_blockchain_transaction_receipt(transaction_hash)
    if receipt:
        status = receipt.status
        if int(status) == 0:
            return f"Transaction {transaction_hash} was not successful"
        elif int(status) == 1:
            return f"Transaction {transaction_hash} was  successful"
        else:
            return f"Dont understand the status of transaction {transaction_hash}, " \
                   f"ideally status should be 0 or 1 but got {status}"
    else:
        return f"Transaction {transaction_hash} has not been included in a block, please try again after some time."

def get_transaction(transaction_id):
    """
    Get a transaction from the trnsaction-api micro service
    :param transaction_id:
    :type transaction_id: int
    :return:
    """
    get_transaction_url = f"{TRANSACTION_API_URI}/transaction-api/one/{int(transaction_id)}"
    try:
        transaction_res = requests.get(url=get_transaction_url)
        transaction_json = transaction_res.json()
        transaction_data = transaction_json['data']
    except Exception as e:
        logger.info(f"Could not get transaction  using id:{transaction_id}, exception got was {str(e)}")
        return None
    return transaction_data



def get_user_group_with_blockchain_address(address):
    url = f"{USERS_API_URI}/user-groups/blockchain-address/{address}"
    try:
        user_res = requests.get(url, cookies={'role_id': '6'})
        user_json = user_res.json()
        user_data = user_json['data']
    except Exception as e:
        logger.info(f"Could not get user  using address:{address}, exception got was {str(e)}")
        return None
    return user_data


def get_blockchain_account(group_id):
    url = f"{USERS_API_URI}/user-groups/id/{group_id}/"
    response = {}
    try:
        user_group_res = requests.get(url, cookies={'role_id': '6'})
        user_group_json = user_group_res.json()
        user_group_data = user_group_json['data']
        user_group_blockchain_account = user_group_data['blockchain_account']
    except Exception as e:
        response['message'] = str(e)
        return jsonify(response)
    return get_or_add_valid_blockchain_account(user_group_blockchain_account, user_group_id=group_id)


def get_or_add_valid_blockchain_account(user_group_blockchain_account, user_group_id):
    """
    If  blockchain account of user is not valid, generate new blockchain account
    and add it to the  user
    :param user_group_blockchain_account:
    :param user_group_id:
    :param user_id:
    :return:
    """
    if valid_blockchain_account(user_group_blockchain_account):
        return user_group_blockchain_account
    else:
        return attach_valid_blockchain_account(user_group_id)


def attach_valid_blockchain_account(user_group_id):
    new_blockchain_account = make_new_account()
    edit_user_group_url = f"{USERS_API_URI}/user-groups/edit-group/"
    new_user_edit = {'group_id': user_group_id,
                     'blockchain_address': new_blockchain_account['address'],
                     'blockchain_private_key': new_blockchain_account['private_key']
                     }
    http = urllib3.PoolManager()
    headers = {'Cookie': 'role_id=6'}
    r = http.request('PUT', edit_user_group_url, fields=new_user_edit, headers=headers)
    logger.info(f'Request for updating user with invalid blockchain account has status: {r.status}')
    return None


def valid_blockchain_account(blockchain_account):
    values = blockchain_account.values()
    for item in values:
        if item == "":  # "" is the default value in the users_database
            return False
    return True


def get_balance(group_id):
    """
    Get balance of a group id
    :param group_id:
    :type group_id str
    :return: dictionary containing available and ledger balance
    """
    balance_url = f"{ACCOUNT_API_URI}/account/one/{group_id}/"
    balance_res = requests.get(url=balance_url, cookies={'role_id': '6'})
    balance_json = balance_res.json()
    try:
        balance_data = balance_json["data"]
    except KeyError:
        logger.info(f"Could not get data from json, json gotten was {balance_json}, "
                    f"request made was get request to {balance_url}")
        return None
    try:
        available_bal, ledger_bal = balance_data["available_balance"], balance_data["ledger_balance"]
    except KeyError:
        logger.info("Could not get available_balance and ledger_balance")
        return None
    return {'available_balance': available_bal, "ledger_balance": ledger_bal}


def enough_balance(group_id, amount):
    """
    Does group with ID have enough balance
    :param group_id: unique id used to identify group
    :param amount:
    :return:
    """
    balance = get_balance(group_id)
    if balance:
        starting_available_bal, starting_ledger_bal = balance['available_balance'], balance['ledger_balance']
        ending_available_bal = starting_available_bal - amount

        if ending_available_bal < 0:
            ending_ledger_bal = starting_ledger_bal + ending_available_bal
            if ending_ledger_bal < 0:
                return False
        return True
    return False


def randomword(length=10):
   return ''.join(random.choice(ascii_letters) for i in range(length))


def debit_group(group_id, amount):
    """
    Debit a group's account with the amount
    :param group_id: unique_id used to find group
    :type group_id: str
    :param amount: amount in naira to debit account with
    :type amount: int, str or float
    :return: Boolean showing status of transaction
    """
    group_debit_url = f"{ACCOUNT_API_URI}/account/blockchain-debit/"
    try:
        res = requests.post(url=group_debit_url, data={"group_id": group_id, "amount": float(amount)})
    except Exception as e:
        logger.info(f"Exception gotten when trying to debit group: {str(e)}. Parameters passed were: {str(locals())}")
        return False
    logger.info(f"Blockchain Debit transaction:{res.json()}")
    return res.json()['success']


def credit_group(group_id, amount, is_working_balance=True):
    """
    Credit a group's account with the amount
    :param group_id: unique_id used to find group
    :type group_id: str
    :param amount_paid: amount in naira to debit account with
    :type amount_paid: int, str or float
    :param is_working_balance: Info required to determine if also credit the main account balance
    :type is_working_balance: boolean
    :return: Boolean showing status of transaction
    """
    group_debit_url = f"{ACCOUNT_API_URI}/account/blockchain-credit/"
    try:
        res = requests.post(url=group_debit_url, data={"group_id": group_id, "amount_paid": amount, 'reference': randomword(), 'is_working_balance': is_working_balance})
    except Exception as e:
        logger.info(f"Exception gotten when trying to credit group was: {str(e)}. Parameters passed were: {str(locals())}")
        return False
    logger.info(f"Blockchain credit Transaction:{res.json()}")
    return res.json()['success']




if __name__ == "__main__":
    # print(get_blockchain_transaction_status("0xcbb60cb160254845bcbac8879b3de08de3f57056232bf99b9315c3682f1fd5b6"))
    # print(get_blockchain_transaction_status("0xcd9ce307f935bca56c9453202c92cb689046feaec3ef78f9cc703e6363c21dd5"))
    # print(get_blockchain_transaction_status("0x77f6e762b28a02747f39a6265943a5fc6f258098d8d57995e8cf97440b351983"))
    # print(enough_balance("lTgYSeRxqcsjkjmz", 800))
    print(credit_group(group_id="9b4db4a0-776a-49cb-8c1b-1caf7fe88f9c", amount=500))
    print(debit_group(group_id="9b4db4a0-776a-49cb-8c1b-1caf7fe88f9c", amount=100))
