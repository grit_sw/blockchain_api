FROM alpine:3.7

ENV FLASK_APP run_api.py

WORKDIR /home/user/

COPY requirements.txt requirements.txt
RUN apk update
RUN apk add python3
RUN apk add --virtual .build-deps \
    gcc \
    python3-dev \
    musl-dev \
    libffi-dev && \
    python3 -m venv venv && \
    venv/bin/pip install -r requirements.txt --no-cache-dir && \
    apk --purge del .build-deps

COPY grit_blockchain_api ./grit_blockchain_api
COPY  config.py logger.py listener.py run_api.py utils.py  start.sh ./

ENV FLASK_ENV Development

RUN chmod +x start.sh

EXPOSE 5000
ENTRYPOINT ["./start.sh"]
